﻿using System;
using NUnit.Framework;
using RobotWalkabout;

namespace RobotWalkaboutTest
{
    [TestFixture]
    public class RobotTest
    {
        [Test]
        public void Senario1()
        {
            Robot robot = new Robot(0, 2, Robot.Heading.East);
            robot.MovementInstructions("MLMRMMMRMMRR");
            Assert.AreEqual(4, robot.GetX(), "xPosition");
            Assert.AreEqual(1, robot.GetY(), "yPosition");
            Assert.AreEqual(0, robot.GetPenalties(), "penalties");
            Assert.AreEqual(Robot.Heading.North, robot.GetHeading(), "headings");
        }

        [Test]
        public void Senario2()
        {
            Robot robot = new Robot(4, 4, Robot.Heading.South);
            robot.MovementInstructions("LMLLMMLMMMRMM");
            Assert.AreEqual(0, robot.GetX(), "xPosition");
            Assert.AreEqual(1, robot.GetY(), "yPosition");
            Assert.AreEqual(1, robot.GetPenalties(), "penalties");
            Assert.AreEqual(Robot.Heading.West, robot.GetHeading(), "headings");
        }

        [Test]
        public void Senario3()
        {
            Robot robot = new Robot(2, 2, Robot.Heading.West);
            robot.MovementInstructions("MLMLMLMRMRMRMRM");
            Assert.AreEqual(2, robot.GetX(), "xPosition");
            Assert.AreEqual(2, robot.GetY(), "yPosition");
            Assert.AreEqual(0, robot.GetPenalties(), "penalties");
            Assert.AreEqual(Robot.Heading.North, robot.GetHeading(), "headings");
        }

        [Test]
        public void Senario4()
        {
            Robot robot = new Robot(1, 3, Robot.Heading.North);
            robot.MovementInstructions("MMLMMLMMMMM");
            Assert.AreEqual(0, robot.GetX(), "xPosition");
            Assert.AreEqual(0, robot.GetY(), "yPosition");
            Assert.AreEqual(3, robot.GetPenalties(), "penalties");
            Assert.AreEqual(Robot.Heading.South, robot.GetHeading(), "headings");
        }
    }
}
