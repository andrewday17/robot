﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RobotWalkabout
{
    public class Robot
    {
        int _xcoordinate;
        int _ycoordinate;
        Heading _heading;
        int _penalty;

        public Robot(int xcoordinate, int ycoordinate, Heading heading)
        {
            this._xcoordinate = xcoordinate;
            this._ycoordinate = ycoordinate;
            this._heading = heading;
            this._penalty = 0;
        }

        public void MovementInstructions(string input)
        {
            for (int i = 0; i < input.Length; i++)
            {
                var tempX = _xcoordinate;
                var tempY = _ycoordinate;
                if (input[i] == 'L' || input[i] == 'R')
                {
                    _heading = Rotation(_heading, input[i]);
                }
                else //must be M = move
                {
                    if (_heading == Heading.North || _heading == Heading.South)
                    {
                        tempY += _heading == Heading.North ? 1 : -1;
                        if (tempY >= 0 && tempY < 5)
                        {
                            _ycoordinate = tempY;
                        }
                        else
                        {
                            _penalty++;
                        }
                    }
                    else //must be Heading.West or Heading.East
                    {
                        tempX += _heading == Heading.East ? 1 : -1;
                        if (tempX >= 0 && tempX < 5)
                        {
                            _xcoordinate = tempX;
                        }
                        else
                        {
                            _penalty++;
                        }
                    }
                }
            }
        }

        private Heading Rotation(Heading heading, char rotationalInstruction)
        {
            if (rotationalInstruction == 'L' && heading == Heading.North)
            {
                heading = Heading.West;
            }
            else if (rotationalInstruction == 'R' && heading == Heading.West)
            {
                heading = Heading.North;
            }
            else
            {
                heading += rotationalInstruction == 'R' ? 1 : -1;
            }
            return heading;
        }

        public int GetX()
        {
            return _xcoordinate;
        }

        public int GetY()
        {
            return _ycoordinate;
        }

        public int GetPenalties()
        {
            return _penalty;
        }

        public Heading GetHeading()
        {
            return _heading;
        }

        public enum Heading
        {
            North = 1,
            East = 2,
            South = 3,
            West = 4
        }
    }
}


